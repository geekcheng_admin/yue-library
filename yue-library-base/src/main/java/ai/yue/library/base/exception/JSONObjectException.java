package ai.yue.library.base.exception;

/**
 * @author  孙金川
 * @version 创建时间：2018年2月3日
 */
public class JSONObjectException extends RuntimeException{
	
	private static final long serialVersionUID = 2014231090943024156L;

	public JSONObjectException(String message) {
		super(message);
	}
	
}
