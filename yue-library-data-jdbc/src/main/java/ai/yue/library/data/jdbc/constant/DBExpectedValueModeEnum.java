package ai.yue.library.data.jdbc.constant;

/**
 * 预期值方式枚举
 * 
 * @author 	孙金川
 * @version 创建时间：2018年9月18日
 */
public enum DBExpectedValueModeEnum {

	等于, 
	大于等于;

}
